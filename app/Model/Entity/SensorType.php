<?php declare(strict_types=1);


namespace App\Model\Entity;

use Swoft\Db\Annotation\Mapping\Column;
use Swoft\Db\Annotation\Mapping\Entity;
use Swoft\Db\Annotation\Mapping\Id;
use Swoft\Db\Eloquent\Model;

/**
 * Class SensorType
 * @package App\Model\Entity
 * @Entity("sensor_type")
 */
class SensorType extends Model
{
    protected const UPDATED_AT = 'updatedDate';

    protected const CREATED_AT = 'createdDate';
    /**
     * @Id(incrementing=false)
     * @Column(name="type_name")
     * @var string
     */
    private $type_name;
    /**
     * @Column(name="data_type")
     * @var string
     */
    private $data_type;
    /**
     * @Column(name="unit")
     * @var string|null
     */
    private $unit;
    /**
     * @Column(name="describe")
     * @var string|null
     */
    private  $describe;
    /**
     * @Column(name="updatedDate")
     * @var string|null
     */
    private $updatedDate;
    /**
     * @Column(name="createdDate")
     * @var string
     */
    private $createdDate;

    /**
     * @return string|null
     */
    public function getUpdatedDate(): ?string
    {
        return $this->updatedDate;
    }

    /**
     * @param string|null $updatedDate
     */
    public function setUpdatedDate(?string $updatedDate): void
    {
        $this->updatedDate = $updatedDate;
    }

    /**
     * @return string
     */
    public function getCreatedDate(): string
    {
        return $this->createdDate;
    }

    /**
     * @param string $createdDate
     */
    public function setCreatedDate(string $createdDate): void
    {
        $this->createdDate = $createdDate;
    }



    /**
     * @return string
     */
    public function getType_name(): string
    {
        return $this->type_name;
    }

    /**
     * @param string $type_name
     */
    public function setType_name(string $type_name): void
    {
        $this->type_name = $type_name;
    }

    /**
     * @return string
     */
    public function getData_type(): string
    {
        return $this->data_type;
    }

    /**
     * @param string $data_type
     */
    public function setData_type(string $data_type): void
    {
        $this->data_type = $data_type;
    }

    /**
     * @return string|null
     */
    public function getUnit(): ?string
    {
        return $this->unit;
    }

    /**
     * @param string|null $unit
     */
    public function setUnit(?string $unit): void
    {
        $this->unit = $unit;
    }

    /**
     * @return string|null
     */
    public function getDescribe(): ?string
    {
        return $this->describe;
    }

    /**
     * @param string|null $describe
     */
    public function setDescribe(?string $describe): void
    {
        $this->describe = $describe;
    }


}
