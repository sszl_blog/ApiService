<?php declare(strict_types=1);


namespace App\Model\Entity;

use Swoft\Db\Annotation\Mapping\Column;
use Swoft\Db\Annotation\Mapping\Entity;
use Swoft\Db\Annotation\Mapping\Id;
use Swoft\Db\Eloquent\Model;

/**
 * Class ProductSensorType
 * @package App\Model\Entity
 * @Entity("product_sensor_type")
 */
class ProductSensorType extends Model
{

    /**
     * @Id()
     * @Column(name="id")
     * @var int
     */
    private $id;
    /**
     * @Column(name="productId")
     * @var string
     */
    private $productId;
    /**
     * @Column(name="type_name")
     * @var string
     */
    private $type_name;



    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getProductId(): string
    {
        return $this->productId;
    }

    /**
     * @param string $productId
     */
    public function setProductId(string $productId): void
    {
        $this->productId = $productId;
    }

    /**
     * @return string
     */
    public function getType_name(): string
    {
        return $this->type_name;
    }

    /**
     * @param string $type_name
     */
    public function setType_name(string $type_name): void
    {
        $this->type_name = $type_name;
    }


}
