<?php declare(strict_types=1);


namespace App\Model\Entity;

use Swoft\Db\Annotation\Mapping\Column;
use Swoft\Db\Annotation\Mapping\Entity;
use Swoft\Db\Annotation\Mapping\Id;
use Swoft\Db\Eloquent\Model;

/**
 * Class Device
 * @package App\Model\Entity
 * @Entity("token")
 */
class Token extends Model
{

    /**
     * @Id()
     * @Column(name="id")
     * @var int
     */
    private $id;

    /**
     *  @Column(name="UserName")
     * @var string
     */
    private $UserName;
    /**
     * @Column(name="token")
     * @var string
     */
    private $token;



    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }



    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken(string $token): void
    {
        $this->token = $token;
    }



    /**
     * @return string
     */
    public function getUserName(): string
    {
        return $this->UserName;
    }

    /**
     * @param string $UserName
     */
    public function setUserName(string $UserName): void
    {
        $this->UserName = $UserName;
    }


}
