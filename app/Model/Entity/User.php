<?php declare(strict_types=1);


namespace App\Model\Entity;

use Swoft\Db\Annotation\Mapping\Column;
use Swoft\Db\Annotation\Mapping\Entity;
use Swoft\Db\Annotation\Mapping\Id;
use Swoft\Db\Eloquent\Model;

/**
 * Class Device
 * @package App\Model\Entity
 * @Entity("user")
 */
class User extends Model
{
    protected const UPDATED_AT = 'updatedDate';

    protected const CREATED_AT = 'createdDate';
    /**
     * @Id()
     * @Column(name="userid")
     * @var int
     */
    private $userid;
    /**
     *  @Column(name="UserName")
     * @var string
     */
    private $UserName;
    /**
     * @Column(name="pwd")
     * @var string
     */
    private $pwd;
    /**
     * @Column(name="authority")
     * @var int
     */
    private $authority;
    /**
     * @Column(name="signature")
     * @var string|null
     */
    private $signature;
    /**
     * @Column(name="email")
     * @var string|null
     */
    private  $email;
    /**
     * @Column(name="updatedDate")
     * @var string|null
     */
    private $updatedDate;
    /**
     * @Column(name="createdDate")
     * @var string
     */
    private $createdDate;

    /**
     * @return string|null
     */
    public function getUpdatedDate(): ?string
    {
        return $this->updatedDate;
    }

    /**
     * @param string|null $updatedDate
     */
    public function setUpdatedDate(?string $updatedDate): void
    {
        $this->updatedDate = $updatedDate;
    }

    /**
     * @return string
     */
    public function getCreatedDate(): string
    {
        return $this->createdDate;
    }

    /**
     * @param string $createdDate
     */
    public function setCreatedDate(string $createdDate): void
    {
        $this->createdDate = $createdDate;
    }

    /**
     * @return int
     */
    public function getUserid(): int
    {
        return $this->userid;
    }

    /**
     * @param int $userid
     */
    public function setUserid(int $userid): void
    {
        $this->userid = $userid;
    }

    /**
     * @return string
     */
    public function getUserName(): string
    {
        return $this->UserName;
    }

    /**
     * @param string $UserName
     */
    public function setUserName(string $UserName): void
    {
        $this->UserName = $UserName;
    }

    /**
     * @return string
     */
    public function getPwd(): string
    {
        return $this->pwd;
    }

    /**
     * @param string $pwd
     */
    public function setPwd(string $pwd): void
    {
        $this->pwd = $pwd;
    }

    /**
     * @return int
     */
    public function getAuthority(): int
    {
        return $this->authority;
    }

    /**
     * @param int $authority
     */
    public function setAuthority(int $authority): void
    {
        $this->authority = $authority;
    }

    /**
     * @return string|null
     */
    public function getSignature(): ?string
    {
        return $this->signature;
    }

    /**
     * @param string|null $signature
     */
    public function setSignature(?string $signature): void
    {
        $this->signature = $signature;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     */
    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }


}
