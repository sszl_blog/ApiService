<?php


namespace App\Model\Entity;

use Swoft\Db\Annotation\Mapping\Column;
use Swoft\Db\Annotation\Mapping\Entity;
use Swoft\Db\Annotation\Mapping\Id;
use Swoft\Db\Eloquent\Model;

/**
 * Class DeviceCmd
 * @package App\Model\Entity
 * @Entity("device_cmd")
 */

class DeviceCmd extends Model
{
    /**
     * @Id()
     * @Column(name="id")
     * @var int
     */
    private $id;
    /**
     * @Column(name="deviceId")
     * @var string
     */
    private  $deviceId;
    /**
     * @Column(name="cmd")
     * @var string
     */
    private  $cmd;
    /**
     * @Column(name="NB_cmdId")
     * @var string|null
     */
    private  $NB_cmdId;
    /**
     * @Column(name="return")
     * @var string|null
     */
    private  $return;
    /**
     * @Column(name="resultCode")
     * @var string|null
     */
    private $resultCode;
    /**
     * @Column(name="sendDate")
     * @var string
     */
    private  $sendDate;


    /**
     * @Column(name="recDate")
     * @var string|null
     */
    private  $recDate;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getDeviceId(): string
    {
        return $this->deviceId;
    }

    /**
     * @param string $deviceId
     */
    public function setDeviceId(string $deviceId): void
    {
        $this->deviceId = $deviceId;
    }

    /**
     * @return string
     */
    public function getCmd(): string
    {
        return $this->cmd;
    }

    /**
     * @param string $cmd
     */
    public function setCmd(string $cmd): void
    {
        $this->cmd = $cmd;
    }

    /**
     * @return string|null
     */
    public function getNB_cmdId(): ?string
    {
        return $this->NB_cmdId;
    }

    /**
     * @param string|null $NB_cmdId
     */
    public function setNB_cmdId(?string $NB_cmdId): void
    {
        $this->NB_cmdId = $NB_cmdId;
    }

    /**
     * @return string|null
     */
    public function getReturn(): ?string
    {
        return $this->return;
    }

    /**
     * @param string|null $return
     */
    public function setReturn(?string $return): void
    {
        $this->return = $return;
    }

    /**
     * @return string
     */
    public function getSendDate(): string
    {
        return $this->sendDate;
    }

    /**
     * @param string $sendDate
     */
    public function setSendDate(string $sendDate): void
    {
        $this->sendDate = $sendDate;
    }

    /**
     * @return string|null
     */
    public function getRecDate(): ?string
    {
        return $this->recDate;
    }

    /**
     * @param string|null $recDate
     */
    public function setRecDate(?string $recDate): void
    {
        $this->recDate = $recDate;
    }

    /**
     * @return string|null
     */
    public function getResultCode(): ?string
    {
        return $this->resultCode;
    }

    /**
     * @param string|null $resultCode
     */
    public function setResultCode(?string $resultCode): void
    {
        $this->resultCode = $resultCode;
    }


}
