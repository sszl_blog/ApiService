<?php declare(strict_types=1);


namespace App\Model\Entity;

use Swoft\Db\Annotation\Mapping\Column;
use Swoft\Db\Annotation\Mapping\Entity;
use Swoft\Db\Annotation\Mapping\Id;
use Swoft\Db\Eloquent\Model;

/**
 * Class SensorData
 * @package App\Model\Entity
 * @Entity("sensor_data")
 */
class SensorData extends Model
{

    /**
     * @Id()
     * @Column(name="id")
     * @var int
     */
    private $id;
    /**
     * @Column(name="sourceId")
     * @var int
     */
    private $sourceId;
    /**
     * @Column(name="deviceId")
     * @var string
     */
    private $deviceId;
    /**
     * @Column(name="productId")
     * @var string
     */
    private $productId;
    /**
     * @Column(name="type_name")
     * @var string
     */
    private $type_name;

    /**
     * @Column(name="data")
     * @var string
     */
    private $data;
    /**
     * @Column(name="date")
     * @var string
     */
    private $date;
    /**
     * @Column(name="SensorNum")
     * @var int
     */
    private $SensorNum;
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getSourceId(): int
    {
        return $this->sourceId;
    }

    /**
     * @param int $sourceId
     */
    public function setSourceId(int $sourceId): void
    {
        $this->sourceId = $sourceId;
    }

    /**
     * @return string
     */
    public function getDeviceId(): string
    {
        return $this->deviceId;
    }

    /**
     * @param string $deviceId
     */
    public function setDeviceId(string $deviceId): void
    {
        $this->deviceId = $deviceId;
    }

    /**
     * @return string
     */
    public function getProductId(): string
    {
        return $this->productId;
    }

    /**
     * @param string $productId
     */
    public function setProductId(string $productId): void
    {
        $this->productId = $productId;
    }

    /**
     * @return string
     */
    public function getType_name(): string
    {
        return $this->type_name;
    }

    /**
     * @param string $type_name
     */
    public function setType_name(string $type_name): void
    {
        $this->type_name = $type_name;
    }

    /**
     * @return string
     */
    public function getData(): string
    {
        return $this->data;
    }

    /**
     * @param string $data
     */
    public function setData(string $data): void
    {
        $this->data = $data;
    }

    /**
     * @return string
     */
    public function getDate(): string
    {
        return $this->date;
    }

    /**
     * @param string $date
     */
    public function setDate(string $date): void
    {
        $this->date = $date;
    }

    /**
     * @return int
     */
    public function getSensorNum(): int
    {
        return $this->SensorNum;
    }

    /**
     * @param int $SensorNum
     */
    public function setSensorNum(int $SensorNum): void
    {
        $this->SensorNum = $SensorNum;
    }


}
