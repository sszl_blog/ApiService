<?php declare(strict_types=1);


namespace App\Model\Entity;

use Swoft\Db\Annotation\Mapping\Column;
use Swoft\Db\Annotation\Mapping\Entity;
use Swoft\Db\Annotation\Mapping\Id;
use Swoft\Db\Eloquent\Model;

/**
 * Class Sensor
 * @package App\Model\Entity
 * @Entity("sensor")
 */
class Sensor extends Model
{
    /**
     * @Id()
     * @Column(name="id",hidden=true)
     * @var int
     */
    private $id;
    /**
     * @Column(name="date")
     * @var string
     */
    private $date;
    /**
     * @Column(name="deviceId")
     * @var string
     */
    private  $deviceId;
    /**
     * @Column(name="SignalPower")
     * @var int|null
     */
    private $SignalPower;
    /**
     * @Column(name="ECL")
     * @var int|null
     */
    private $ecl;
    /**
     * @Column(name="SNR")
     * @var int|null
     */
    private $snr;
    /**
     * @Column(name="CellID")
     * @var int|null
     */
    private $CellID;
    /**
     * @Column(name="battery")
     * @var int|null
     */
    private $battery;
    /**
     * @Column(name="data")
     * @var string|null
     */
    private $data;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getDate(): string
    {
        return $this->date;
    }

    /**
     * @param string $date
     */
    public function setDate(string $date): void
    {
        $this->date = $date;
    }

    /**
     * @return string
     */
    public function getDeviceId(): string
    {
        return $this->deviceId;
    }

    /**
     * @param string $deviceId
     */
    public function setDeviceId(string $deviceId): void
    {
        $this->deviceId = $deviceId;
    }

    /**
     * @return int|null
     */
    public function getSignalPower(): ?int
    {
        return $this->SignalPower;
    }

    /**
     * @param int|null $SignalPower
     */
    public function setSignalPower(?int $SignalPower): void
    {
        $this->SignalPower = $SignalPower;
    }

    /**
     * @return int|null
     */
    public function getEcl(): ?int
    {
        return $this->ecl;
    }

    /**
     * @param int|null $ecl
     */
    public function setEcl(?int $ecl): void
    {
        $this->ecl = $ecl;
    }

    /**
     * @return int|null
     */
    public function getSnr(): ?int
    {
        return $this->snr;
    }

    /**
     * @param int|null $snr
     */
    public function setSnr(?int $snr): void
    {
        $this->snr = $snr;
    }

    /**
     * @return int|null
     */
    public function getCellID(): ?int
    {
        return $this->CellID;
    }

    /**
     * @param int|null $CellID
     */
    public function setCellID(?int $CellID): void
    {
        $this->CellID = $CellID;
    }

    /**
     * @return int|null
     */
    public function getBattery(): ?int
    {
        return $this->battery;
    }

    /**
     * @param int|null $battery
     */
    public function setBattery(?int $battery): void
    {
        $this->battery = $battery;
    }

    /**
     * @return string|null
     */
    public function getData(): ?string
    {
        return $this->data;
    }

    /**
     * @param string|null $data
     */
    public function setData(?string $data): void
    {
        $this->data = $data;
    }


}
