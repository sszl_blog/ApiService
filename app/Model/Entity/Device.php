<?php declare(strict_types=1);


namespace App\Model\Entity;

use Swoft\Db\Annotation\Mapping\Column;
use Swoft\Db\Annotation\Mapping\Entity;
use Swoft\Db\Annotation\Mapping\Id;
use Swoft\Db\Eloquent\Model;

/**
 * Class Device
 * @package App\Model\Entity
 * @Entity("device")
 */
class Device extends Model
{
    protected const UPDATED_AT = 'updatedDate';

    protected const CREATED_AT = 'createdDate';
    /**
     * @Id(incrementing=false)
     * @Column(name="deviceId")
     * @var string
     */
    private $deviceId;
    /**
     *  @Column(name="model")
     * @var string
     */
    private $model;
    /**
     * @Column(name="userid")
     * @var int
     */
    private $userid;

    /**
     * @Column(name="productId")
     * @var string
     */
    private $productId;
    /**
     * @Column(name="key")
     * @var string
     */
    private $key;
    /**
     * @Column(name="isSecure")
     * @var bool
     */
    private $isSecure;
    /**
     * @Column(name="imei")
     * @var string|null
     */
    private  $imei;
    /**
     * @Column(name="psk")
     * @var string|null
     */
    private  $psk;
    /**
     * @Column(name="Nb_deviceId")
     * @var string|null
     */
    private  $Nb_deviceId;

    /**
     * @Column(name="updatedDate")
     * @var string|null
     */
    private $updatedDate;
    /**
     * @Column(name="createdDate")
     * @var string
     */
    private $createdDate;

    /**
     * @return string|null
     */
    public function getUpdatedDate(): ?string
    {
        return $this->updatedDate;
    }

    /**
     * @param string|null $updatedDate
     */
    public function setUpdatedDate(?string $updatedDate): void
    {
        $this->updatedDate = $updatedDate;
    }

    /**
     * @return string
     */
    public function getCreatedDate(): string
    {
        return $this->createdDate;
    }

    /**
     * @param string $createdDate
     */
    public function setCreatedDate(string $createdDate): void
    {
        $this->createdDate = $createdDate;
    }



    /**
     * @return string
     */
    public function getDeviceId(): string
    {
        return $this->deviceId;
    }

    /**
     * @param string $deviceId
     */
    public function setDeviceId(string $deviceId): void
    {
        $this->deviceId = $deviceId;
    }

    /**
     * @return string
     */
    public function getProductId(): string
    {
        return $this->productId;
    }

    /**
     * @param string $productId
     */
    public function setProductId(string $productId): void
    {
        $this->productId = $productId;
    }

    /**
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * @param string $key
     */
    public function setKey(string $key): void
    {
        $this->key = $key;
    }

    /**
     * @return string|null
     */
    public function getImei(): ?string
    {
        return $this->imei;
    }

    /**
     * @param string|null $imei
     */
    public function setImei(?string $imei): void
    {
        $this->imei = $imei;
    }

    /**
     * @return string|null
     */
    public function getPsk(): ?string
    {
        return $this->psk;
    }

    /**
     * @param string|null $psk
     */
    public function setPsk(?string $psk): void
    {
        $this->psk = $psk;
    }

    /**
     * @return string|null
     */
    public function getNb_deviceId(): ?string
    {
        return $this->Nb_deviceId;
    }

    /**
     * @param string|null $Nb_deviceId
     */
    public function setNb_deviceId(?string $Nb_deviceId): void
    {
        $this->Nb_deviceId = $Nb_deviceId;
    }

    /**
     * @return string
     */
    public function getModel(): string
    {
        return $this->model;
    }

    /**
     * @param string $model
     */
    public function setModel(string $model): void
    {
        $this->model = $model;
    }

    /**
     * @return bool
     */
    public function isSecure(): bool
    {
        return $this->isSecure;
    }

    /**
     * @param bool $isSecure
     */
    public function setIsSecure(bool $isSecure): void
    {
        $this->isSecure = $isSecure;
    }

    /**
     * @return int
     */
    public function getUserid(): int
    {
        return $this->userid;
    }

    /**
     * @param int $userid
     */
    public function setUserid(int $userid): void
    {
        $this->userid = $userid;
    }


}
