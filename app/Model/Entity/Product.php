<?php declare(strict_types=1);


namespace App\Model\Entity;

use Swoft\Db\Annotation\Mapping\Column;
use Swoft\Db\Annotation\Mapping\Entity;
use Swoft\Db\Annotation\Mapping\Id;
use Swoft\Db\Eloquent\Model;

/**
 * Class Product
 * @package App\Model\Entity
 * @Entity("product")
 */
class Product extends Model
{
    protected const UPDATED_AT = 'updatedDate';

    protected const CREATED_AT = 'createdDate';
    /**
     * @Id(incrementing=false)
     * @Column(name="productId")
     * @var string
     */
    private $productId;
    /**
     * @Column(name="productName")
     * @var string
     */
    private $productName;
    /**
     * @Column(name="describe")
     * @var string|null
     */
    private $describe;
    /**
     * @Column(name="userid")
     * @var int
     */
    private $userid;

    /**
     * @Column(name="updatedDate")
     * @var string|null
     */
    private $updatedDate;
    /**
     * @Column(name="createdDate")
     * @var string
     */
    private $createdDate;

    /**
     * @return string|null
     */
    public function getUpdatedDate(): ?string
    {
        return $this->updatedDate;
    }

    /**
     * @param string|null $updatedDate
     */
    public function setUpdatedDate(?string $updatedDate): void
    {
        $this->updatedDate = $updatedDate;
    }

    /**
     * @return string
     */
    public function getCreatedDate(): string
    {
        return $this->createdDate;
    }

    /**
     * @param string $createdDate
     */
    public function setCreatedDate(string $createdDate): void
    {
        $this->createdDate = $createdDate;
    }



    /**
     * @return string
     */
    public function getProductId(): ?string
    {
        return $this->productId;
    }

    /**
     * @param string $productId
     */
    public function setProductId(?string $productId): void
    {
        $this->productId = $productId;
    }

    /**
     * @return string|null
     */
    public function getDescribe(): ?string
    {
        return $this->describe;
    }

    /**
     * @param string|null $describe
     */
    public function setDescribe(?string $describe): void
    {
        $this->describe = $describe;
    }



    /**
     * @return string
     */
    public function getProductName(): string
    {
        return $this->productName;
    }

    /**
     * @param string $productName
     */
    public function setProductName(string $productName): void
    {
        $this->productName = $productName;
    }

    /**
     * @return int
     */
    public function getUserid(): int
    {
        return $this->userid;
    }

    /**
     * @param int $userid
     */
    public function setUserid(int $userid): void
    {
        $this->userid = $userid;
    }


}
