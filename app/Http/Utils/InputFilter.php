<?php


namespace App\Http\Utils;

/**
 * Class InputFilter
 * 输入验证
 * @package App\Http\Utils
 */
class InputFilter
{
    /**
     * 过滤参数
     * @param string $str 接受的参数
     * PHP防XSS 防SQL注入的代码
     * @return string
     */
    static public function filterWords($str)
    {
        $farr = array(
            "/<(\\/?)(script|i?frame|style|html|body|title|link|meta|object|\\?|\\%)([^>]*?)>/isU",
            "/(<[^>]*)on[a-zA-Z]+\s*=([^>]*>)/isU",
            "/select|insert|update|delete|\'|\/\*|\*|\.\.\/|\.\/|union|into|load_file|outfile|dump/is"
        );
        $str = preg_replace($farr,'',$str);
        return $str;
    }

    /**
     * PHP防XSS 防SQL注入的代码
     * 过滤接受的参数或者数组,如$_GET,$_POST
     * @param array|string $arr 接受的参数或者数组
     * @return array|string
     */
    static public function filterArr($arr)
    {
        if(is_array($arr)){
            foreach($arr as $k => $v){
                $arr[$k] = self::filterWords($v);
            }
        }else{
            $arr = self::filterWords($arr);
        }
        return $arr;
    }


    /**
     * @param $str
     * 防止sql注入的函数，过滤掉那些非法的字符,提高sql安全性，同时也可以过滤XSS的攻击。
     * @return bool|mixed|string
     */
    static  public function filter($str)
    {
        if (empty($str)) return false;
        $str = htmlspecialchars($str);
        $str = str_replace( '/', "", $str);
        $str = str_replace( '"', "", $str);
        $str = str_replace( '(', "", $str);
        $str = str_replace( ')', "", $str);
        $str = str_replace( 'CR', "", $str);
        $str = str_replace( 'ASCII', "", $str);
        $str = str_replace( 'ASCII 0x0d', "", $str);
        $str = str_replace( 'LF', "", $str);
        $str = str_replace( 'ASCII 0x0a', "", $str);
        $str = str_replace( ',', "", $str);
        $str = str_replace( '%', "", $str);
        $str = str_replace( ';', "", $str);
        $str = str_replace( 'eval', "", $str);
        $str = str_replace( 'open', "", $str);
        $str = str_replace( 'sysopen', "", $str);
        $str = str_replace( 'system', "", $str);
        $str = str_replace( '$', "", $str);
        $str = str_replace( "'", "", $str);
        $str = str_replace( "'", "", $str);
        $str = str_replace( 'ASCII 0x08', "", $str);
        $str = str_replace( '"', "", $str);
        $str = str_replace( '"', "", $str);
        $str = str_replace("", "", $str);
        $str = str_replace("&gt", "", $str);
        $str = str_replace("&lt", "", $str);
        $str = str_replace("<SCRIPT>", "", $str);
        $str = str_replace("</SCRIPT>", "", $str);
        $str = str_replace("<script>", "", $str);
        $str = str_replace("</script>", "", $str);
        $str = str_replace("select","",$str);
        $str = str_replace("join","",$str);
        $str = str_replace("union","",$str);
        $str = str_replace("where","",$str);
        $str = str_replace("insert","",$str);
        $str = str_replace("delete","",$str);
        $str = str_replace("update","",$str);
        $str = str_replace("like","",$str);
        $str = str_replace("drop","",$str);
        $str = str_replace("DROP","",$str);
        $str = str_replace("create","",$str);
        $str = str_replace("modify","",$str);
        $str = str_replace("rename","",$str);
        $str = str_replace("alter","",$str);
        $str = str_replace("cas","",$str);
        $str = str_replace("&","",$str);
        $str = str_replace(">","",$str);
        $str = str_replace("<","",$str);
        $str = str_replace(" ",chr(32),$str);
        $str = str_replace(" ",chr(9),$str);
        $str = str_replace("    ",chr(9),$str);
        $str = str_replace("&",chr(34),$str);
        $str = str_replace("'",chr(39),$str);
        $str = str_replace("<br />",chr(13),$str);
        $str = str_replace("''","'",$str);
        $str = str_replace("css","'",$str);
        $str = str_replace("CSS","'",$str);
        $str = str_replace("<!--","",$str);
        $str = str_replace("convert","",$str);
        $str = str_replace("md5","",$str);
        $str = str_replace("passwd","",$str);
        $str = str_replace("password","",$str);
        $str = str_replace("../","",$str);
        $str = str_replace("./","",$str);
        $str = str_replace("Array","",$str);
        $str = str_replace("or 1='1'","",$str);
        $str = str_replace(";set|set&set;","",$str);
        $str = str_replace("`set|set&set`","",$str);
        $str = str_replace("--","",$str);
        $str = str_replace("OR","",$str);
        $str = str_replace('"',"",$str);
        $str = str_replace("*","",$str);
        $str = str_replace("-","",$str);
        $str = str_replace("+","",$str);
        $str = str_replace("/","",$str);
        $str = str_replace("=","",$str);
        $str = str_replace("'/","",$str);
        $str = str_replace("-- ","",$str);
        $str = str_replace(" -- ","",$str);
        $str = str_replace(" --","",$str);
        $str = str_replace("(","",$str);
        $str = str_replace(")","",$str);
        $str = str_replace("{","",$str);
        $str = str_replace("}","",$str);
        $str = str_replace("-1","",$str);
        $str = str_replace("1","",$str);
        $str = str_replace(".","",$str);
        $str = str_replace("response","",$str);
        $str = str_replace("write","",$str);
        $str = str_replace("|","",$str);
        $str = str_replace("`","",$str);
        $str = str_replace(";","",$str);
        $str = str_replace("etc","",$str);
        $str = str_replace("root","",$str);
        $str = str_replace("//","",$str);
        $str = str_replace("!=","",$str);
        $str = str_replace("$","",$str);
        $str = str_replace("&","",$str);
        $str = str_replace("&&","",$str);
        $str = str_replace("==","",$str);
        $str = str_replace("#","",$str);
        $str = str_replace("@","",$str);
        $str = str_replace("mailto:","",$str);
        $str = str_replace("CHAR","",$str);
        $str = str_replace("char","",$str);
        return $str;
    }

    static  public function inputFiltre($name)
    {
        if (!get_magic_quotes_gpc()) // 判断magic_quotes_gpc是否为打开
        {
            $post = addslashes($name); // magic_quotes_gpc没有打开的时候把数据过滤
        }

        $name = str_replace("_", "\_", $name); // 把 '_'过滤掉

        $name = str_replace("%", "\%", $name); // 把' % '过滤掉

        $name = nl2br($name); // 回车转换

        $name = htmlspecialchars($name); // html标记转换

        return $name;
    }

}
