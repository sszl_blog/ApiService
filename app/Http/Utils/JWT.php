<?php declare(strict_types=1);


namespace App\Http\Utils;

use App\Model\Entity\Token;
use Swoft\Bean\Exception\ContainerException;
use Swoft\Db\Exception\DbException;

/**
 * Class JWT
 *
 * @since 2.0
 */
class Jwt {

    //头部
    private static $header=array(
        'alg'=>'HS256', //生成signature的算法
        'typ'=>'JWT'  //类型
    );

    //使用HMAC生成信息摘要时所使用的密钥 使用配置文件
    //private static $key='123456';


    /**
     * 获取jwt token
     * @param $user string 用户名
     * [
     * 'iss'=>'jwt_admin', //该JWT的签发者
     * 'iat'=>time(), //签发时间
     * 'exp'=>time()+7200, //过期时间
     * 'nbf'=>time()+60, //该时间之前不接收处理该Token
     * 'sub'=>'www.admin.com', //面向的用户
     * 'aud' = > 'user' 用户
     * 'jti'=>md5(uniqid('JWT').time()) //该Token唯一标识
     * ]
     * @return bool|string
     */
    public static function getToken($user)
    {
        if($user == "")
            return false;
        $payload = array(
            "iss"=> \config("JWTAdmin","admin") ,
            'iat'=>time(),
            'exp'=>time()+\config("TokenOutTime",7200),
            'nbf'=>time(),
            'sub'=>\config("JWTsub","sszlbg.cn"),
            'aud'=>$user,
            'jti'=>md5(uniqid('JWT').time())
        );
        if(is_array($payload))
        {
            $base64header=self::base64UrlEncode(json_encode(self::$header,JSON_UNESCAPED_UNICODE));
            $base64payload=self::base64UrlEncode(json_encode($payload,JSON_UNESCAPED_UNICODE));
            $token=$base64header.'.'.$base64payload.'.'.self::signature($base64header.'.'.$base64payload,\config("JWTKey","123456"),self::$header['alg']);
            //服务器保存
            $Table_Token = null;
            try
            {
                $Table_Token = Token::where("UserName",$user);

                if($Table_Token->get()->count() > 0) {
                    try {
                        $Table_Token->update(["token" => $token]);
                    } catch (\ReflectionException $e) {
                        return false;
                    } catch (ContainerException $e) {
                        return false;
                    }
                }
                else {
                    try {
                        Token::insert(["UserName" => $user, "token" => $token]);
                    } catch (\ReflectionException $e) {
                        return false;
                    } catch (ContainerException $e) {
                        return false;
                    }
                }


            } catch (DbException $e) {
                return false;
            }


            return $token;
        }else{
            return false;
        }
    }


    /**
     * 验证token是否有效,默认验证exp,nbf,iat时间
     * @param string $token 需要验证的token
     * @return int|array -1 $token验证失败 -2 $token过期 -3用户已登出 -4服务器错误
     */
    public static function verifyToken(string $token)
    {
        $tokens = explode('.', $token);
        if (count($tokens) != 3)
            return -1;

        list($base64header, $base64payload, $sign) = $tokens;

        //获取jwt算法
        $base64decodeheader = json_decode(self::base64UrlDecode($base64header), true);
        if (empty($base64decodeheader['alg']))
            return -1;

        //签名验证
        if (self::signature($base64header . '.' . $base64payload, \config("JWTKey","123456"), $base64decodeheader['alg']) !== $sign)
            return -1;

        $payload = json_decode(self::base64UrlDecode($base64payload), true);

        //签发时间大于当前服务器时间验证失败
        if (isset($payload['iat']) && $payload['iat'] > time())
            return -1;

        //过期时间小宇当前服务器时间验证失败
        if (isset($payload['exp']) && $payload['exp'] < time())
            return -2;

        //该nbf时间之前不接收处理该Token
        if (isset($payload['nbf']) && $payload['nbf'] > time())
            return -1;
        if(!isset($payload['aud']))
            return -1;

        //授权有效

        //判断是否退出
        try{
            $Token = Token::where("UserName",$payload["aud"]);
            if($Token->get()->count() <= 0)//不存在验证失败
                return -3;
            if($Token->get()[0]["token"] != $token)//存在验证是否一致
                return -2;
        } catch (DbException $e) {
            return -4;
        }




        return $payload;
    }




    /**
     * base64UrlEncode  https://jwt.io/ 中base64UrlEncode编码实现
     * @param string $input 需要编码的字符串
     * @return string
     */
    private static function base64UrlEncode(string $input)
    {
        return str_replace('=', '', strtr(base64_encode($input), '+/', '-_'));
    }

    /**
     * base64UrlEncode https://jwt.io/ 中base64UrlEncode解码实现
     * @param string $input 需要解码的字符串
     * @return bool|string
     */
    private static function base64UrlDecode(string $input)
    {
        $remainder = strlen($input) % 4;
        if ($remainder) {
            $addlen = 4 - $remainder;
            $input .= str_repeat('=', $addlen);
        }
        return base64_decode(strtr($input, '-_', '+/'));
    }

    /**
     * HMACSHA256签名  https://jwt.io/ 中HMACSHA256签名实现
     * @param string $input 为base64UrlEncode(header).".".base64UrlEncode(payload)
     * @param string $key
     * @param string $alg  算法方式
     * @return mixed
     */
    private static function signature(string $input, string $key, string $alg = 'HS256')
    {
        $alg_config=array(
            'HS256'=>'sha256'
        );
        return self::base64UrlEncode(hash_hmac($alg_config[$alg], $input, $key,true));
    }
}
